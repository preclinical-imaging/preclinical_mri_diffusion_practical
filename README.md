# preclinical_MRI_Diffusion_Practical

Preclinical MRI practical - part of the MRI Graduate course 2021-2022. If you have any questions, please contact
Cristiana Tisca (cristiana.tisca@linacre.ox.ac.uk) or Aurea Martins-Bach (aurea.martins-bach@ndcn.ox.ac.uk).

Please do not share this material outside of the WIN Graduate Course.
